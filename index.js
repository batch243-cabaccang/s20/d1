let i = 5;

while (i != 0) {
  console.log(i);
  i--;
}

do {
  console.log(i);
  i++;
} while (i <= 5);

for (i = 0; i < 10; i++) {
  if (i % 2 === 0) {
    continue;
  } else {
    console.log(i);
  }
}


